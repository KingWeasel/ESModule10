/*
   pwm_gen.ino

   Jacob Kunnappally
   November 2017

   What this program does:

   Setup
   -defines, globals, the usual

   Runtime
   -Setup PWM through timer and usecond wait (for duty cycle)
   -Flash LED according to PWM ISR
*/

//works from values between 0.1Hz and 75.0kHz
//(I didn't have the patience to find a lower limit)
#define PWM_FREQ 1000.34567

//works from values between 8.0% and 80.0%
//(exactly 0% and 100% also)
#define PWM_DUTY 100

#define OUT_PIN 7

HardwareTimer timer(1);
bool led_val = true;

int period_us, duty_wait_us;

void setup()
{
  pinMode(OUT_PIN, OUTPUT);

  configure_pwm();
}

void loop()
{}

void handler_led(void)
{
  digitalWrite(OUT_PIN, true);
  delayMicroseconds(duty_wait_us);
  digitalWrite(OUT_PIN, false);
}

void configure_pwm()
{
  // Pause the timer while we're configuring it
  timer.pause();

  // Set up period
  period_us = (1.0 / PWM_FREQ) * 1000000;
  duty_wait_us = (PWM_DUTY / 100.0) * period_us;
  timer.setPeriod(period_us); // in microseconds

  // Set up an interrupt on channel 2
  timer.setMode(TIMER_CH2, TIMER_PWM);
  timer.attachInterrupt(TIMER_CH2, handler_led);

  // Refresh the timer's count, prescale, and overflow
  timer.refresh();
  // Start the timer counting
  timer.resume();
}







