/*
   pwm_dac.ino

   Jacob Kunnappally
   October 2017

   What this program does:

   Setup
   -defines, globals, serial init. the usual
   -configure pin 7 for pulse-width modulation (PWM) output
   -construct lookup table (LUT) for a sine wave

   Runtime
   -keep changing PWM output according to sine LUT
   (implementation is fixed at 10Hz output sine wave)

*/

#define OUT_PIN 7
#define MAX_DUTY_CYCLE 65535
#define SINE_SIZE 64

int sine[SINE_SIZE];

void setup()
{
  pinMode(OUT_PIN, PWM);//from Fading.ino

  // initialize serial
  Serial.begin(115200);
  while (!Serial);
  Serial.flush();
  Serial.println("Starting...");

  make_sin_LUT();
  //  pwmWrite(OUT_PIN, 32768);
}

void loop()
{
  int i = 0;

  for (; i < SINE_SIZE; i++)
  {
    pwmWrite(OUT_PIN, sine[i]);
    delayMicroseconds(500);//~10Hz, really 1.5625ms
  }
}

void make_sin_LUT()
{
  double range = 2 * PI;
  double range_step = range / SINE_SIZE;
  float gain_and_bias = MAX_DUTY_CYCLE / 2;
  float primitive_sine[SINE_SIZE];

  for (int i = 0; i < SINE_SIZE; i++)
  {
    primitive_sine[i] = gain_and_bias * sin(i * range_step) + gain_and_bias;
    sine[i] = (int)primitive_sine[i];
    Serial.println(sine[i]);
  }
}



